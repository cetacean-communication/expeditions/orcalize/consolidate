# Consolidation of RawData into PublishData

This git repository   
[https://gitlab.switch.ch/cetacean-communication/expeditions/orcalize/consolidate.git](https://gitlab.switch.ch/cetacean-communication/expeditions/orcalize/consolidate.git)  
is accompanying the data publication:   

**Underwater sounds, including killer whale and humpback whale vocalizations, recorded in northern Norway in January 2023**

[DOI: 10.5281/zenodo.7657352](https://doi.org/10.5281/zenodo.7657352)

See the file info/info.pdf for a detailed description of the data publication. 

The jupyter-lab notebooks with python scripts were used to consolidate the raw data collected on the expedition "Orcalize". The raw data consists of hydrophone recordings, drone videos, and GPS tracks of the ship as they are stored by the instruments. The consolidation (or compilation) process has several steps: 
* Omit recordings with bad quality or no animal vocalizations. For example if the noise from ships are to dominant, or when there is a strong cable strum because the ship drifted to fast in the wind, we do not include the recording in the publication. 
* Trim the recordings. In case the recording started when the hydrophones were not yet in the water, or when the recording did not stop at the end and records voices on board of the ship. 
* Apply the calibration of the hydrophones and the gain setting of the recorder. This yields a quantitative measurement of sound pressure in the units of Pa. 
* The recorder saves separate wav files for every channel, and "rolls over" when the files get to long (2 GB limit of wav files). We merge all the files of a continuous recording into a single file (HDF5). 
* The geographic information into projected to a local cartesian coordinate system. The geographic GPS tracks in GPX files (latitude, longitude) are transformed to a stereographic local coordinate system for easier geometric calculations. 
* Adding metadata to each file providing all the necessary information for further analysis.
     
The scripts provided in this repository read from the folder *RawData* and store the consolidated files into the folder *PublishData*. This code is published along with the *PublishData* for documentation purposes and reproducibility. Since the *RawData* will not be published, the consolidation process can, of course, not be reproduced, unless you request the *RawData*.     

## Installation
JupyterLab with Python 3.10. See the file CondaEnv.yml for a list of packages that are needed. Run the following conda commands to create the environment:  
conda env create -f CondaEnv.yml  
conda activate CondaEnv   
  
## Usage
**Explore.jpynb:** This notebook provides examples to load the data and plot some basic graphs to validate the data. Routines to export the data to other file formats, for example wav files for analysis in RavenPro. 

**CompileData.jpynb:** This juypter-lab notebook was used to consolidate the raw data into published data.

**CompileExcursion.jpynb:** This notebook documents how the GPS tracks of the ship were consolidated.

The last two notebooks are provided for documentation only. The raw data folder is not published and therefore the notebook serves for documentation only.

## Author
Jörg Rychen  
jrychen@ethz.ch  
Institute of Neuroinformatics  
University of Zurich and ETH Zurich  
Winterthurerstrasse 190  
CH-8057 Zurich  
Switzerland  

## License
GNU 3 

## Project status
considered complete with the publication of the data. 
